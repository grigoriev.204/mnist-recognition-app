import os
import settings

from aiohttp import web

from recognizer import Recognizer

routes = web.RouteTableDef()

image_recognizer = Recognizer()

def static_resource(resource_name):
    return os.path.join(settings.STATIC_DIR, resource_name)


@routes.get("/")
async def index(request):
    """
    Returns the main page of application
    """
    return web.FileResponse(static_resource("index.html"))


@routes.post("/recognize")
async def recognize(request):
    """
    Recognize digit from image and send response to front-end
    """
    data = await request.read()
    digit = await image_recognizer.recognize(data)
    return web.json_response({"digit": digit})

async def main():
    app = web.Application()
    app.add_routes(routes)
    app.router.add_static("/js/", static_resource("js/"), name="js")
    app.router.add_static("/css/", static_resource("css/"), name="css")
    app.router.add_static("/", static_resource(""), name="common_static")
    return app


if __name__ == "__main__":
    app = main()
    web.run_app(app, host=settings.HOSTNAME, port=settings.PORT)
