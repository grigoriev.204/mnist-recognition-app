const canvasHandling = {
    el: '#app',
    data () {
        return {
            pos: {x: 0, y: 0},
            recognizedDigit: null,
            recognizeLabel: "Recognize",
            isRecognizeDisabled: false
        }
    },

    mounted: function() {
        const canvas = this.$refs.canvasInput;
        const ctx = canvas.getContext('2d');
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        ctx.stroke();
    },

    methods: {

        canvasMouseMove ($event) {
            if ($event.buttons !== 1) {return;}
            const ctx = this.$refs.canvasInput.getContext('2d');
            ctx.lineWidth = 20;
            ctx.lineCap = 'round';
            ctx.strokeStyle = "black";
            ctx.beginPath();
            let pos = this.$data.pos;
            ctx.moveTo(pos.x, pos.y);
            this.setPosition($event);
            pos = this.$data.pos;
            ctx.lineTo($event.offsetX, $event.offsetY);
            ctx.stroke();
            ctx.closePath();
        },

        setPosition($event) {
            var bbox = this.$refs.canvasInput.getBoundingClientRect();
            this.$data.pos.x = $event.clientX - bbox.left;
            this.$data.pos.y = $event.clientY - bbox.top;
        },

       async recognizeDigit ($event) {
            this.isRecognizeDisabled = true;
            this.recognizeLabel = "Recognizing...";
            const canvas = this.$refs.canvasInput;
            let blobImage = await new Promise(resolve => canvas.toBlob(resolve, "image/png"));
            let response = await fetch('/recognize', {
                method: 'POST',
                body: blobImage
              })
            let result = await response.json();
            this.$data.recognizedDigit = result.digit;
            this.isRecognizeDisabled = false;
            this.recognizeLabel = "Recognize";
        },

        clearCanvas ($event) {
        const canvas = this.$refs.canvasInput;
        const ctx = canvas.getContext('2d');
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        this.$data.recognizedDigit = null;
        }
    }
}

app = Vue.createApp(canvasHandling);
var vm = app.mount("#app");