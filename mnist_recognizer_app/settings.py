import os

STATIC_DIR = os.path.join(os.path.dirname(__file__), "static/")
HOSTNAME = "localhost"
PORT = 8000

RECOGNITION_MODEL_PATH = "mnist_recognizer_app/recognizer/mnist_histogram_gradient_boosting.model"