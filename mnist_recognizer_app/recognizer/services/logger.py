import logging

from recognizer.abstractservices import AbstractLogger


class DefaultLogger(AbstractLogger):
    """
    Default logger implementation
    """

    def __init__(self, logger_name=__name__, log_file="log.txt"):
        self.__log = logging.getLogger(logger_name)
        self.__log.setLevel(logging.DEBUG)
        handler = logging.FileHandler(log_file, "w", "utf-8")
        self.__log.addHandler(handler)

    def info(self, message) -> None:
        self.__log.info(message)

    def warning(self, message: str) -> None:
        self.__log.warning(message)

    def error(self, message: str) -> None:
        self.__log.error(message)