import numpy as np

from io import BytesIO
from PIL import Image, ImageOps
from recognizer.abstractservices import AbstractPreprocessor

class MnistImagePreprocessor(AbstractPreprocessor):
    """
    Convert image to 28x28 pic and invert the colors.
    """

    MNIST_IMAGE_SIZE = (28, 28)

    def preprocess(self, data: bytes) -> np.ndarray:
        f = BytesIO(data)
        image = Image.open(f)
        resized_image = image.resize(MnistImagePreprocessor.MNIST_IMAGE_SIZE)
        greyscale_image = resized_image.convert("L")
        inverted_image = ImageOps.invert(greyscale_image)
        result = np.asarray(inverted_image).reshape(1, 784)
        return result