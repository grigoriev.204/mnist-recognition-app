import pickle

from recognizer.abstractservices import AbstractModelLoader, AbstractModel


class PickledModelLoader(AbstractModelLoader):
    """
    Load skikit-learn model pickled to file.
    """
    def load(self, model_path: str) -> AbstractModel:
        model = None
        try:
            with open(model_path, "rb") as f:
                model = pickle.load(f)
        except Exception as e:
            raise Exception("Failed to load model from %s." % model_path)
        return model