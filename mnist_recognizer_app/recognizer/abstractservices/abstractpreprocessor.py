import numpy as np
from abc import ABC, abstractmethod

class AbstractPreprocessor(ABC):
    """
    Defines data preprocessor interface.
    """
    @abstractmethod
    def preprocess(self, data) -> np.ndarray:
        pass
