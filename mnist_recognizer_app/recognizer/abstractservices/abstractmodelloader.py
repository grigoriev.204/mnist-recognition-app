from abc import ABC, abstractmethod
from . import AbstractModel

class AbstractModelLoader(ABC):
    """
    Defines Model loader interface
    """
    @abstractmethod
    def load(self, model_path: str) -> AbstractModel:
        pass