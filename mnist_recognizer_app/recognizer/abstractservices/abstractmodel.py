import numpy as np

from abc import ABC, abstractmethod

class AbstractModel(ABC):
    """
    Defines machine learning Model interface.
    """
    @abstractmethod
    def predict(x: np.ndarray) -> np.ndarray:
        pass

    @abstractmethod
    def score(x_test: np.ndarray, y_test: np.ndarray) -> float:
        pass