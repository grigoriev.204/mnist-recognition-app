from abc import ABC, abstractmethod

class AbstractLogger(ABC):
    """
    Defines logger interface.
    """
    
    @abstractmethod
    def info(message: str) -> None:
        pass

    @abstractmethod
    def warning(message: str) -> None:
        pass

    @abstractmethod
    def error(message: str) -> None:
        pass