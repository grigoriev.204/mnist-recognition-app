import os

RECOGNITION_MODEL_PATH = os.path.join(os.path.dirname(__file__), "models", "mnist_histogram_gradient_boosting.model")\
                         or os.environ["RECOGNITION_MODEL_PATH"]
