from recognizer.services import PickledModelLoader, DefaultLogger, MnistImagePreprocessor
from recognizer.abstractservices import AbstractModelLoader, AbstractLogger, AbstractPreprocessor
from .settings import RECOGNITION_MODEL_PATH


class Recognizer(object):
    """
    Provide recognize() method to recognize digit. Also encapsulates ModelLoader, Logger and Preprocessor instances.
    """
    def __init__(self,
                 model_loader=PickledModelLoader(),
                 logger=DefaultLogger(),
                 preprocessor=MnistImagePreprocessor()) -> None:
        self.__model_loader = model_loader
        self.__logger = logger
        self.__preprocessor = preprocessor
        super().__init__()

    @property
    def model_loader(self) -> AbstractModelLoader:
        return self.__model_loader

    @model_loader.setter
    def model_loader(self, loader: AbstractModelLoader):
        self.__model_loader = loader

    @property
    def logger(self):
        return self.__logger

    @logger.setter
    def logger(self, logger: AbstractLogger):
        self.__logger = logger

    @property
    def preprocessor(self):
        return self.__preprocessor

    @preprocessor.setter
    def preprocessor(self, preprocessor: AbstractPreprocessor):
        self.__preprocessor = preprocessor

    async def recognize(self, data: bytes) -> int:
        """
        Preprocess input data.
        Load ML model.
        Calculate ML model on input data.
        Return calculation results.
        """
        X = self.__preprocessor.preprocess(data)
        prediction_model = self.__model_loader.load(RECOGNITION_MODEL_PATH)
        self.logger.info("Recognition with %s model" % RECOGNITION_MODEL_PATH)
        result = [-1]
        if prediction_model is not None:
            result = prediction_model.predict(X)
        return int(result[0])

