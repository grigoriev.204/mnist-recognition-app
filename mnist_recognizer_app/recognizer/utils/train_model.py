import pandas as pd
import pickle

from sklearn.model_selection import train_test_split
from sklearn.ensemble import HistGradientBoostingClassifier


print("Load training data.")
X_mnist = pd.read_csv("mnist_recognizer_app/recognizer/utils/mnist_X.csv")
y_mnist = pd.read_csv("mnist_recognizer_app/recognizer/utils/mnist_Y.csv")

print("Split to train/test.")
X_train, X_test, y_train, y_test = train_test_split(X_mnist.to_numpy()[:, 1:], y_mnist["class"], test_size = 0.25, random_state=611)

classifier = HistGradientBoostingClassifier(
    loss='categorical_crossentropy',
    learning_rate=0.1,
    max_iter=150,
    max_leaf_nodes=31,
    max_depth=None,
    min_samples_leaf=20,
    l2_regularization=0.4,
    max_bins=255,
    categorical_features=None,
    monotonic_cst=None,
    warm_start=False,
    early_stopping='auto',
    scoring='loss',
    validation_fraction=0.1,
    n_iter_no_change=10,
    tol=1e-07,
    verbose=0,
    random_state=None
)

print("Train model.")
model = classifier.fit(X_train, y_train)
print("Calculate model score.")
score = model.score(X_test, y_test)
print("Model score: %s" % score)
with open("mnist_recognizer_app/recognizer/models/mnist_histogram_gradient_boosting.model", "wb") as f:
    pickle.dump(model, f)