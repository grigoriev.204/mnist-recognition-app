# Handswritten digit recognition app

## Demo
![Recognition demonstration](/images/digit-recognizer.gif "Recognize")


## Machine learning part of app

Recognition model of application is based on `MNIST dataset` (https://www.openml.org/d/554). Recognizer has been trained by `skikit-learn` classification algorithms as example estimator. Model is quite inaccurate in complex cases like choosing between "3" and "8" or "1" and "7". "4" is a best recognizable digit by this model.

## Web app

Web part of this application is SPA based on `Vue` and `aiohttp` libraries.


## App link
[MNIST Recognition app](https://mnist-recognition-app.herokuapp.com/)

